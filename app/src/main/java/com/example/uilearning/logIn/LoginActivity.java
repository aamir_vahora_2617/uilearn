package com.example.uilearning.logIn;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.uilearning.util.ImageUrl;
import com.example.uilearning.R;
import com.squareup.picasso.Picasso;

public class LoginActivity extends AppCompatActivity {

    private ImageView imgBackground;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        imgBackground = findViewById(R.id.imgBackground);




        Picasso.with(imgBackground.getContext()).load(ImageUrl.loginUrl).fit().into(imgBackground);






    }
}