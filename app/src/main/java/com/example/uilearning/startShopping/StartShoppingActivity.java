package com.example.uilearning.startShopping;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.uilearning.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class StartShoppingActivity extends AppCompatActivity {

    private ConstraintLayout rootLayout;
    private TextView txtHomeCraft,txtMessage;
    private Button btnStartShopping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_shopping);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        initView();

        Picasso.with(this).load("https://i.pinimg.com/236x/70/5a/f8/705af8e36ba7c15ff7f3f424ffa85e10.jpg").into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                rootLayout.setBackground(new BitmapDrawable(bitmap));
                txtHomeCraft.setVisibility(View.VISIBLE);
                txtMessage.setVisibility(View.VISIBLE);
                btnStartShopping.setVisibility(View.VISIBLE);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    private void initView()
    {
        rootLayout = findViewById(R.id.rootLayout);
        txtHomeCraft = findViewById(R.id.txtHomeCraft);
        txtMessage = findViewById(R.id.txtMessage);
        btnStartShopping = findViewById(R.id.btnStartShopping);
    }
}