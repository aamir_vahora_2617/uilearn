package com.example.uilearning.aboutUs;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.uilearning.R;
import com.example.uilearning.util.ImageUrl;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class AboutUsActivity extends AppCompatActivity {

    private ConstraintLayout topLayout;
    private ImageView imgBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        initView();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);


        Picasso.with(topLayout.getContext()).load("https://github.com/aamir2617/UIlearningFolder/blob/master/about_background.jpg?raw=true").into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                topLayout.setBackgroundDrawable(new BitmapDrawable(bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        Picasso.with(this).load(ImageUrl.aboutBottomUrl).fit().into(imgBottom);




    }

    private void initView() {

        topLayout = findViewById(R.id.topLayout);
        imgBottom = findViewById(R.id.imgBottom);
    }
}