package com.example.uilearning.designers;

public class DesignerDetails {

    String name,description,city,country,imageUrl;

    public DesignerDetails() {
    }

    public DesignerDetails(String name, String description, String city, String country, String imageUrl) {
        this.name = name;
        this.description = description;
        this.city = city;
        this.country = country;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
