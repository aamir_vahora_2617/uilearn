package com.example.uilearning.designers;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uilearning.R;
import com.example.uilearning.util.ImageUrl;

import java.util.ArrayList;
import java.util.List;

public class DesignerActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DesignersAdapter designersAdapter;
    private List<DesignerDetails> list = new ArrayList<>();
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setStatusBarColor(Color.parseColor("#f4f4f4"));
        setContentView(R.layout.activity_designer);

        initView();

        setSupportActionBar(toolbar);

        list.add(new DesignerDetails("Kane Steve","Function and shape become more than just the sum of parts","New York","USA", ImageUrl.designer_oneUrl));
        list.add(new DesignerDetails("Kane Steve","Function and shape become more than just the sum of parts","New York","USA", ImageUrl.designer_oneUrl));
        list.add(new DesignerDetails("Kane Steve","Function and shape become more than just the sum of parts","New York","USA", ImageUrl.designer_oneUrl));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        designersAdapter = new DesignersAdapter(this,list);
        recyclerView.setAdapter(designersAdapter);
    }

    private void initView() {

        recyclerView = findViewById(R.id.recyclerView);
        toolbar = findViewById(R.id.toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu,menu);
        return true;
    }
}