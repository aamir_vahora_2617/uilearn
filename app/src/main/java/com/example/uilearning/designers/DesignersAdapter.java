package com.example.uilearning.designers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uilearning.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DesignersAdapter extends RecyclerView.Adapter<DesignersAdapter.MyHolder> {

    Context context;
    List<DesignerDetails> list = new ArrayList<>();

    public DesignersAdapter(Context context, List<DesignerDetails> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.designer_item,parent,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        DesignerDetails designerDetails = list.get(position);
        holder.onBind(designerDetails);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView txtName,txtCity,txtCountry,txtDescription;
        ImageView imgProfile;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtCity = itemView.findViewById(R.id.txtCity);
            txtCountry = itemView.findViewById(R.id.txtCountry);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            imgProfile = itemView.findViewById(R.id.imgProfile);
        }

        public void onBind(DesignerDetails designerDetails) {

            txtName.setText(designerDetails.getName());
            txtDescription.setText(designerDetails.getDescription());
            txtCountry.setText(designerDetails.getCountry());
            txtCity.setText(designerDetails.getCity());

            Picasso.with(imgProfile.getContext()).load(designerDetails.getImageUrl()).fit().into(imgProfile);
            imgProfile.setClipToOutline(true);

        }
    }
}
