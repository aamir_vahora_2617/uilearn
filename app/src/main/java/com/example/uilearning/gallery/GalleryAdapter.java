package com.example.uilearning.gallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uilearning.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyHolder> {

    Context context;
    List<Chairs> chairList = new ArrayList<>();

    public GalleryAdapter(Context context, List<Chairs> chairList) {
        this.context = context;
        this.chairList = chairList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chair_item,parent,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        Chairs chairs = chairList.get(position);

        holder.onBind(chairs);

    }

    @Override
    public int getItemCount() {
        return chairList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView txtTitle,txtDescription;
        ImageView imgItem;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            imgItem = itemView.findViewById(R.id.imgItem);
        }

        public void onBind(Chairs chairs) {

            txtTitle.setText(chairs.getName());
            txtDescription.setText(chairs.getDescription());

            Picasso.with(imgItem.getContext()).load(chairs.getImageUrl()).into(imgItem);

        }
    }
}
