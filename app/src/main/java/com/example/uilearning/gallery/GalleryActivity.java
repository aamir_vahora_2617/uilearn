package com.example.uilearning.gallery;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uilearning.R;
import com.example.uilearning.util.ImageUrl;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity {

    private ConstraintLayout layoutBackground;
    private RecyclerView rview;
    private List<Chairs> chairList = new ArrayList<>();
    private GalleryAdapter adapter;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);



        setContentView(R.layout.activity_gallery);

        initView();

        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;

        ViewGroup.MarginLayoutParams params= (ViewGroup.MarginLayoutParams) toolbar.getLayoutParams();
        params.topMargin=statusBarHeight+20;



        setSupportActionBar(toolbar);



        Picasso.with(this).load(ImageUrl.galleryUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                layoutBackground.setBackground(new BitmapDrawable(bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        chairList.add(new Chairs("Chairs","Beautifully resolved furniture",ImageUrl.gallery_chairUrl));
        chairList.add(new Chairs("Tables","Beautifully resolved furniture",ImageUrl.gallery_tablesUrl));
        chairList.add(new Chairs("Sofa","Beautifully resolved furniture",ImageUrl.gallery_sofasUrl));
        chairList.add(new Chairs("Arm chairs","Beautifully resolved furniture",ImageUrl.gallery_armchairUrl));



        rview.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        adapter = new GalleryAdapter(this,chairList);
        rview.setAdapter(adapter);
        rview.setLeft(0);


    }

    private void initView() {

        layoutBackground = findViewById(R.id.layoutBackground);
        rview  = findViewById(R.id.rview);
        toolbar = findViewById(R.id.toolbar2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu,menu);
        return true;
    }

}